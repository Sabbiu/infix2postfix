#include <iostream>
#include <cstdio>

using namespace std;
const int MAX = 50;

class stack //push, pop or peeks the value of stack
{
    char stk[MAX];
public:
    int top;
    stack(){ top = -1; }
    void push(char val){ stk[++top] = val; }
    char pop(){ return stk[top--]; }
    char peek(){ return stk[top]; }
}Stack;

//char op[] = {'-', '+', '*', '/', '^'}; //higher value for higher precedence operator
//int precedence(char temp) //returns the precedence
//{
//    for(int i = 0; i < 5; i++)
//        if(op[i] == temp)   return i;
//    return -1;
//}
int precedence(char temp) //returns the precedence
{
    switch(temp){
        case '^': return 3;
        case '/': return 2;
        case '*': return 2;
        case '+': return 1;
        case '-': return 1;
        default: return -1;
    }
}


int main()
{
    char infix[MAX], postfix[MAX]; // stores infix and postfix expression
    int inMax = -1, postMax = -1; // Max length for corresponding expression
    int prev_oper = -1, now_oper;

    cout << "Enter infix expression : ";
    while((infix[++inMax] = getchar())!='\n'); //inputs infix expression
    infix[inMax] = ')'; // placing right parenthesis at end

    Stack.push('('); //pushes left parenthesis to stack

    for(int i = 0; i <= inMax; i++){
        if((infix[i] >= 'A' && infix[i] <= 'Z') ||  //checking for uppercase alphabets or
           (infix[i] >= 'a' && infix[i] <= 'z'))    //checking for lowercase alphabets
                postfix[++postMax] = infix[i];

        else if(infix[i] == '(')
                Stack.push('('); //pushes left parenthesis to stack

        else if(infix[i] == ')'){
            while(Stack.peek() != '(')  //pops from stack and puts to postfix exp till right parenthesis is encountered
                postfix[++postMax] = Stack.pop();
            Stack.top--; //removes left parenthesis from stack
        }

        else{
            //returns and stores the precedence of operator
            prev_oper = precedence(Stack.peek());
            now_oper = precedence(infix[i]);
            while((prev_oper != -1) && (now_oper <= prev_oper)){ //checks if there was an operator on the stack and checks precedence of operators
                postfix[++postMax] = Stack.pop();
                prev_oper = precedence(Stack.peek());
            }
            Stack.push(infix[i]);
        }
    }

    cout<<"The postfix expression is : ";
    for(int i=0; i <= postMax; i++)
        cout<<postfix[i];
    return 0;
}